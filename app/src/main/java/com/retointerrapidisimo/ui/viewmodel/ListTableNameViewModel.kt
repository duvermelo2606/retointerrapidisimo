package com.retointerrapidisimo.ui.viewmodel

import androidx.lifecycle.ViewModel
import java.util.*

class ListTableNameViewModel: ViewModel() {

    fun filterList(query: String?, listTableNameOriginal: List<String>): List<String> {
        var filteredList = listOf<String>()
        if (query != null) {
            filteredList = listTableNameOriginal.filter {name ->
                name.lowercase(Locale.ROOT).contains(query.lowercase(Locale.ROOT))
            }
        }

        return filteredList
    }
}