package com.retointerrapidisimo.ui.viewmodel

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.retointerrapidisimo.data.model.DatabaseModel
import com.retointerrapidisimo.data.model.DbHelper
import com.retointerrapidisimo.data.network.ApiResponseStatus
import com.retointerrapidisimo.domain.GetDatabaseUseCase
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {
    var isLoadingDatabase = MutableLiveData<Boolean>()
    var isCreateDatabase = MutableLiveData<Boolean>()

    lateinit var dbHelper: DbHelper

    var getDatabaseUseCase = GetDatabaseUseCase()

    fun onCreate(context: Context) {
        viewModelScope.launch {
            isLoadingDatabase.postValue(true)
            val result = getDatabaseUseCase()

            when(result) {

                is ApiResponseStatus.Error -> {
                    Toast.makeText(context, (result.messageId), Toast.LENGTH_SHORT).show()
                    isCreateDatabase.postValue(false)
                }
                is ApiResponseStatus.Success -> {
                    createDatabase(context, result.data)
                }
            }
            isLoadingDatabase.postValue(false)

        }
    }

    private fun createDatabase(context: Context, listDatabase: List<DatabaseModel>?) {
        dbHelper = DbHelper(context, listDatabase)
        val createDatabase: SQLiteDatabase = dbHelper.writableDatabase
        if (createDatabase != null) {
            isCreateDatabase.postValue(true)
        }else {
            isCreateDatabase.postValue(false)
        }
    }

    fun getNameTablesDatabase(): ArrayList<String> {
        val db: SQLiteDatabase = dbHelper.readableDatabase
        val listTableName = ArrayList<String>()
        val cursor: Cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null)

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast) {
                listTableName.add(cursor.getString(cursor.getColumnIndex("name")))
                cursor.moveToNext()
            }
        }
        return listTableName
    }

    fun isInternetAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

}