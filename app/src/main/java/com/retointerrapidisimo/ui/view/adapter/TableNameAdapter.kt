package com.retointerrapidisimo.ui.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.retointerrapidisimo.R

class TableNameAdapter(private var listTableName: List<String>): RecyclerView.Adapter<TableNameViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TableNameViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return TableNameViewHolder(layoutInflater.inflate(R.layout.item_table_name, parent, false))
    }

    override fun getItemCount(): Int = listTableName.size

    override fun onBindViewHolder(holder: TableNameViewHolder, position: Int) {
        val item = listTableName[position]
        holder.render(item)
    }

}