package com.retointerrapidisimo.ui.view

import android.os.Bundle
import android.widget.SearchView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.retointerrapidisimo.LIST_TABLE
import com.retointerrapidisimo.R
import com.retointerrapidisimo.databinding.ActivityListTableNameBinding
import com.retointerrapidisimo.ui.view.adapter.TableNameAdapter
import com.retointerrapidisimo.ui.viewmodel.ListTableNameViewModel

class ListTableNameActivity : AppCompatActivity() {

    private lateinit var binding: ActivityListTableNameBinding
    private var listTableNameOriginal: List<String> = listOf()
    private val listTableNameViewModel: ListTableNameViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListTableNameBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val extras = intent.extras
        if (extras != null) {
            listTableNameOriginal = extras.getStringArrayList(LIST_TABLE) ?: listOf()
        }

        binding.svTableName.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val listFilteredTemp = listTableNameViewModel.filterList(newText, listTableNameOriginal)
                binding.rvListTablesDatabase.adapter = TableNameAdapter(listFilteredTemp)
                if (listFilteredTemp.isEmpty())
                    Toast.makeText(this@ListTableNameActivity, R.string.no_table_name, Toast.LENGTH_SHORT).show()
                return true
            }
        })

        initListTableName()
    }
    private fun initListTableName() {
        val manager = LinearLayoutManager(this)
        val decoration = DividerItemDecoration(this, manager.orientation)
        binding.rvListTablesDatabase.layoutManager = manager
        binding.rvListTablesDatabase.adapter = TableNameAdapter(listTableNameOriginal)
        binding.rvListTablesDatabase.addItemDecoration(decoration)
    }
}