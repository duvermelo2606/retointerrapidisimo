package com.retointerrapidisimo.ui.view.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.retointerrapidisimo.databinding.ItemTableNameBinding

class TableNameViewHolder(view: View): RecyclerView.ViewHolder(view) {

    private val binding = ItemTableNameBinding.bind(view)
    fun render(nameTable: String) {
        binding.tvNameTable.text = nameTable
    }
}