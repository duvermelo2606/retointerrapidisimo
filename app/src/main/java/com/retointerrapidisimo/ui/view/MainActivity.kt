package com.retointerrapidisimo.ui.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.retointerrapidisimo.LIST_TABLE
import com.retointerrapidisimo.R
import com.retointerrapidisimo.databinding.ActivityMainBinding
import com.retointerrapidisimo.ui.viewmodel.MainViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initButtons()
        initObservables()
    }

    private fun initObservables() {
        mainViewModel.isLoadingDatabase.observe(this) {
            binding.pbLoadDatabase.isVisible = it
        }

        mainViewModel.isCreateDatabase.observe(this) {
            if (it) {
                binding.btnFilterDatabase.visibility = View.VISIBLE
            }else {
                binding.btnLoadDatabase.visibility = View.VISIBLE
                Toast.makeText(this, R.string.error_created_database, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun initButtons() {

        binding.btnLoadDatabase.setOnClickListener {
            if (mainViewModel.isInternetAvailable(this)) {
                binding.btnLoadDatabase.visibility = View.GONE
                mainViewModel.onCreate(this)
            }else {
                Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show()
            }
        }

        binding.btnFilterDatabase.setOnClickListener {
            val intent = Intent(this, ListTableNameActivity::class.java)
            intent.putStringArrayListExtra(LIST_TABLE, mainViewModel.getNameTablesDatabase())
            startActivity(intent)
        }
    }

}