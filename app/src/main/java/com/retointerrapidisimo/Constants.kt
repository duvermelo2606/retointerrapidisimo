package com.retointerrapidisimo

const val BASE_URL = "https://apitesting.interrapidisimo.co/FtAppAgencias012/apiControllerPruebas/api/"
const val USER = "usuario"
const val LIST_TABLE = "listTable"
const val KEY_HEADER_USER = "admin"
const val NEEDS_AUTH_HEADER_KEY = "needs_authentication"