package com.retointerrapidisimo.core

import com.retointerrapidisimo.KEY_HEADER_USER
import com.retointerrapidisimo.NEEDS_AUTH_HEADER_KEY
import com.retointerrapidisimo.USER
import okhttp3.Interceptor
import okhttp3.Response

object ApiServiceInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val requestBuilder = request.newBuilder()
        if (request.header(NEEDS_AUTH_HEADER_KEY) != null) {
            requestBuilder.addHeader(USER, KEY_HEADER_USER)
        }
        return chain.proceed(requestBuilder.build())
    }
}