package com.retointerrapidisimo.data.network

import com.retointerrapidisimo.data.model.DatabaseModel

sealed class ApiResponseStatus() {
    class Success(val data: List<DatabaseModel>): ApiResponseStatus()
    class Error(val messageId: Int): ApiResponseStatus()
}