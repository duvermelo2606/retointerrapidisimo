package com.retointerrapidisimo.data.network

import com.retointerrapidisimo.NEEDS_AUTH_HEADER_KEY
import com.retointerrapidisimo.data.model.DatabaseModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers

interface DatabaseApi {
    @GET("SincronizadorDatos/ObtenerEsquema/true")
    @Headers("${NEEDS_AUTH_HEADER_KEY}: true")
    suspend fun getAllDatabase(): Response<List<DatabaseModel>>
}