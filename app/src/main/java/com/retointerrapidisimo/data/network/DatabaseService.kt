package com.retointerrapidisimo.data.network

import com.retointerrapidisimo.R
import com.retointerrapidisimo.core.RetrofitHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.SocketTimeoutException

class DatabaseService {

    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getDatabase(): ApiResponseStatus {
        return withContext(Dispatchers.IO) {
            try {
                val response = retrofit.create(DatabaseApi::class.java).getAllDatabase()
                ApiResponseStatus.Success(response.body() ?: emptyList())
            }catch (e: SocketTimeoutException) {
                ApiResponseStatus.Error(R.string.low_connection)
            }
            catch (e: Exception) {
                ApiResponseStatus.Error(R.string.download_error)
            }
        }
    }
}