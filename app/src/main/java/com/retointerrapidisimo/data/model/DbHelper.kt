package com.retointerrapidisimo.data.model

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DbHelper(context: Context, listDatabase: List<DatabaseModel>?): SQLiteOpenHelper(context,
    DATABASE_NAME,null,
    DATABASE_VERSION
)  {
    private val listDatabaseDB = listDatabase
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "retoInterrapidisimoBD"

    }

    override fun onCreate(db: SQLiteDatabase?) {
        listDatabaseDB?.forEach { table ->
            db?.execSQL(table.query)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }
}