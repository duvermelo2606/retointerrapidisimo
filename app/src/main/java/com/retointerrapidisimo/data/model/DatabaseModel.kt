package com.retointerrapidisimo.data.model

import com.google.gson.annotations.SerializedName

data class DatabaseModel(
    @SerializedName("NombreTabla") var table_name : String,
    @SerializedName("Pk") var primary_key : String,
    @SerializedName("QueryCreacion") var query : String,
    @SerializedName("BatchSize") var batch_size : String,
    @SerializedName("Filtro") var filter : String,
    @SerializedName("Error") var error : String,
    @SerializedName("NumeroCampos") var fields : String,
    @SerializedName("MetodoApp") var app_method : String,
    @SerializedName("FechaActualizacionSincro") var updated_at : String
)