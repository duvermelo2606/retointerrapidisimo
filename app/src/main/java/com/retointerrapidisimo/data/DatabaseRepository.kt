package com.retointerrapidisimo.data

import com.retointerrapidisimo.data.network.ApiResponseStatus
import com.retointerrapidisimo.data.network.DatabaseService

class DatabaseRepository {
    private val api = DatabaseService()

    suspend fun getDatabase(): ApiResponseStatus = api.getDatabase()
}