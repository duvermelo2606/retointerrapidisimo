package com.retointerrapidisimo.domain

import com.retointerrapidisimo.data.DatabaseRepository
import com.retointerrapidisimo.data.network.ApiResponseStatus

class GetDatabaseUseCase {
    private val repository = DatabaseRepository()

    suspend operator fun invoke(): ApiResponseStatus = repository.getDatabase()
}